<?php

namespace App\Http\Controllers;

use App\Model\Book;
use App\Model\Rent;
use App\Model\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class PeminjamanController extends Controller
{
    public function index(Request $request){
        if ($request->isMethod('post')){
            $data = Rent::with('student')
                ->with('book')
                ->has('returns', '<=', 0)
                ->select("*");
            return DataTables::of($data)->make(true);
        }
        return $this->view();
    }

    public function add(Request $request){
        if ($request->isMethod('post')){
            $this->validate($request, [
                'book_id'       => [
                    'required',
                    'exists:books,id',
                    function($attribute, $value, $fail){
                        $book = Book::find($value);
                        if ($book->status == Book::STATUS_UNAVAILABLE){
                            $fail("$book->code - $book->title tidak tersedia");
                        }
                    }
                ],
                'student_id'    => 'required|exists:students,id',
                'date'          => 'required|date'
            ]);

            $rent = (new Rent())->fill($request->all());
            $days_expired = Setting::get('Max_Rent_Time');
            $rent->expired_date = Carbon::createFromFormat('Y-m-d', $rent->date)
                ->addDays($days_expired);

            $rent->save();

            $rent->book->status = Book::STATUS_UNAVAILABLE;
            $rent->book->save();

            return redirect()->route('rent');
        }

        if ($request->b){
            $book = Book::find($request->b);
            if ($book){
                return redirect()->route('rent.add')->withInput(['book_id' => $request->b]);
            }
        }
        return $this->view();
    }

    public function detail(Request $request, $id){
        $rent = Rent::with('book')->with('student')->find($id);

        if (!$rent)
            return redirect()->route('rent');

        $date = Carbon::createFromFormat("Y-m-d", $rent->expired_date);
        $fine = 0;
        if ($date->diffInDays(Carbon::now(), false) > 0){
            $fine = Carbon::now()->diffInDays($date) * Setting::get('Daily_Fine');
        }

        return $this->view(['rent' => $rent, 'fine' => $fine]);
    }

    public function delete(Request $request){
        $this->validate($request, [
            'id' => 'required',
        ]);

        $rent = Rent::find($request->id);
        $rent->book->status = Book::STATUS_AVAILABLE;
        $rent->book->save();

        $rent->delete();

        return response()->json([
            'status' => true
        ]);
    }
}
