<?php

namespace App\Http\Controllers;

use App\Model\Rent;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $active_rent = Rent::has('returns', '=', 0)->count();
        $today_rent = Rent::where('date', '=', Carbon::now()->format('Y-m-d'))->count();

        $expired = Rent::with('book')
            ->with('student')
            ->where('expired_date', '>=', Carbon::now())
            ->orderBy('expired_date')
            ->take(10)->get();
        return view('home', [
            'expired' => $expired,
            'active_rent' => $active_rent,
            'today_rent' => $today_rent
        ]);
    }
}
