<?php

namespace App\Http\Controllers;

use App\Model\ClassRoom;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Yajra\DataTables\DataTables;

class ClassController extends Controller
{
    public function index(Request $request){
        if ($request->isMethod('post')){
            return DataTables::of(ClassRoom::query())->make(true);
        }
        return $this->view();
    }

    public function add(Request $request){
        $this->validate($request, [
           'name' => 'required|unique:classes',
        ]);

        $class = new ClassRoom();
        $class->name = $request->name;
        $class->save();

        return response()->json([
            'status' => true
        ]);
    }

    public function edit(Request $request){
        $this->validate($request, [
            'id' => 'required',
            'name' => [
                'required',
                Rule::unique('classes')->ignore($request->id)
            ],
        ]);

        $class = ClassRoom::find($request->id);
        $class->name = $request->name;
        $class->save();

        return response()->json([
            'status' => true
        ]);
    }

    public function delete(Request $request){
        $this->validate($request, [
            'id' => 'required',
        ]);

        try{
            $class = ClassRoom::find($request->id);
            $class->delete();
        }catch (\Exception $ex){
            return response()->json([
                'status' => false,
                'message' => 'Data tidak dapat dihapus! Terdapat siswa dikelas ini.'
            ], 500);
        }

        return response()->json([
            'status' => true
        ]);
    }
}
