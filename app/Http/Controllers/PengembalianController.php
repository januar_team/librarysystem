<?php

namespace App\Http\Controllers;

use App\Model\Book;
use App\Model\Rent;
use App\Model\Returns;
use App\Model\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class PengembalianController extends Controller
{
    public function index(Request $request){
        if ($request->isMethod('post')){
            $data = Returns::with('rent')
                ->with('rent.student')
                ->with('rent.book')
                ->select("*");
            return DataTables::of($data)->make(true);
        }
        return $this->view();
    }

    public function add(Request $request, $id){
        $rent = Rent::with('student')
            ->with('book')
            ->find($id);
        if (!$rent)
            return redirect()->route('rent');

        $date = Carbon::createFromFormat("Y-m-d", $rent->expired_date);
        $fine = 0;
        if ($date->diffInDays(Carbon::now(), false) > 0) {
            $fine = Carbon::now()->diffInDays($date) * Setting::get('Daily_Fine');
        }

        if ($request->isMethod('post')){
            $this->validate($request, [
                'rent_id'       => 'required|exists:rents,id',
            ]);

            DB::transaction(function () use($rent, $fine){
                $return = new Returns();
                $return->rent_id = $rent->id;

                $rent->book->status = Book::STATUS_AVAILABLE;
                $rent->book->save();

                $return->denda = $fine;
                $return->save();
            });

            return redirect()->route('return');
        }

        return $this->view(['rent' => $rent, 'fine' => $fine]);
    }
}
