<?php

namespace App\Http\Controllers;

use App\Model\ClassRoom;
use App\Model\Student;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Yajra\DataTables\DataTables;

class StudentController extends Controller
{
    public function index(Request $request){
        if ($request->isMethod('post')){
            $students = Student::join('classes', 'students.class_id' , '=', 'classes.id')
                ->select(['students.*', 'classes.name as class']);
            return DataTables::of($students)->make(true);
        }

        $classes = ClassRoom::get();
        return $this->view(['classes' => $classes]);
    }

    public function add(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'nim'   => 'required|unique:students',
            'class_id'  => 'required'
        ]);

        $student = new Student();
        $student->fill($request->all());
        $student->save();

        return response()->json([
            'status' => true
        ]);
    }

    public function edit(Request $request){
        $this->validate($request, [
            'id' => 'required',
            'name' => 'required',
            'nim' => [
                'required',
                Rule::unique('students')->ignore($request->id)
            ],
            'class_id'  => 'required'
        ]);

        $student = Student::find($request->id);
        $student->name = $request->name;
        $student->nim = $request->nim;
        $student->class_id = $request->class_id;
        $student->save();

        return response()->json([
            'status' => true
        ]);
    }

    public function delete(Request $request){
        $this->validate($request, [
            'id' => 'required',
        ]);

        try{
            $student = Student::find($request->id);
            $student->delete();
        }catch (\Exception $ex){
            return response()->json([
                'status' => false,
                'message' => 'Data tidak dapat dihapus!. Siswa tersebut memiliki data peminjaman.'
            ], 500);
        }

        return response()->json([
            'status' => true
        ]);
    }

    public function search(Request $request){
        $data = Student::with('class_room')->where('nim', 'LIKE', "$request->search%")
            ->paginate(10);
        return response()->json($data);
    }

    public function get(Request $request, $id){
        $student = Student::with('class_room')->find($id);

        return response()->json($student);
    }
}
