<?php

namespace App\Http\Controllers;

use App\Model\Book;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class BookController extends Controller
{
    public function index(Request $request){
        if ($request->isMethod('post')){
            $data = Book::with('rent')->select('*');
                return DataTables::of($data)->make(true);
        }
        return $this->view();
    }

    public function add(Request $request){
        if ($request->isMethod('post')){
            $this->validate($request,[
                'isbn' => 'required',
                'title' => 'required',
                'author' => 'required',
                'publisher' => 'required',
                'code'  =>'required',
                'unit'  => 'required|numeric|min:1'
            ]);

            for ($i = 1; $i <= $request->unit; $i++){
                $book = new Book();
                $book->fill($request->all());
                $book->code = sprintf("%s/%d", $book->code , $i);
                $book->status = Book::STATUS_AVAILABLE;
                $book->save();
            }

            return redirect()->route('book');
        }
        return $this->view();
    }

    public function search(Request $request){
        $data = Book::where('code', 'LIKE', "%$request->search%")
            ->paginate(10);
        return response()->json($data);
    }

    public function get(Request $request, $id){
        $book = Book::find($id);

        return response()->json($book);
    }

    public function delete(Request $request){
        $this->validate($request, [
            'id' => 'required',
        ]);

        try{
            $book = Book::find($request->id);
            $book->delete();
        }catch (\Exception $ex){
            return response()->json([
                'status' => false,
                'message' => 'Data gagal dihapus!',
                'error' => $ex->getMessage()
            ], 500);
        }

        return response()->json([
            'status' => true
        ]);
    }
}
