<?php
/**
 * Created by PhpStorm.
 * User: Januar
 * Date: 11/17/2018
 * Time: 9:07 AM
 */

namespace App\Http\Middleware;


use Closure;
use Lavary\Menu\Menu;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        if($user != null){
            \Menu::make('MyNavBar', function ($menu) use($request){
                $menu->add('Dashboard', ['route' => 'home', 'class' => 'nav-item'])
                    ->prepend('<i class="nav-icon icon-speedometer"></i><span>')
                    ->append('</span>')
                    ->link->attr([
                        'class' =>'nav-link'
                    ]);
                $menu->raw('Master Data',['class' => 'nav-title']);

                $book = $menu->add('Buku', ['route' => 'book', 'class' => 'nav-item'])
                    ->prepend('<i class="nav-icon icon-book-open"></i><span>')
                    ->append('</span>');
                $book->link->attr([
                        'class' =>'nav-link'
                    ]);

                $book->add('Add', ['route' => 'book.add', 'class' => 'breadcrumb-item']);

                $siswa = $menu->add('Data Siswa', ['class' => 'nav-item nav-dropdown'])
                    ->prepend('<i class="nav-icon icon-people"></i><span>')
                    ->append('</span>');
                $siswa->link->attr([
                    'class' =>'nav-link nav-dropdown-toggle',
                    'href' => '#'
                ]);

                $siswa->add('Kelas', ['route' => 'class', 'class' => 'nav-item'])
                    ->prepend('<i class="nav-icon fa fa-circle-o"></i><span>')
                    ->append('</span>')
                    ->link->attr([
                        'class' =>'nav-link'
                    ]);

                $siswa->add('Siswa', ['route' => 'student', 'class' => 'nav-item'])
                    ->prepend('<i class="nav-icon fa fa-circle-o"></i><span>')
                    ->append('</span>')
                    ->link->attr([
                        'class' =>'nav-link'
                    ]);

                $menu->raw('Administrasi',['class' => 'nav-title']);

                $rent = $menu->add('Peminjaman', ['route' => 'rent', 'class' => 'nav-item', 'data-level' => 1])
                    ->prepend('<i class="nav-icon icon-arrow-left-circle"></i><span>')
                    ->append('</span>');
                $rent->link->attr([
                        'class' =>'nav-link'
                    ]);

                $rent->add('Add', ['route' => 'rent.add', 'class' => 'breadcrumb-item']);
                $rent->add('Detail', ['route' => ['rent.detail', 'id' => $request->segment(3)], 'class' => 'breadcrumb-item']);

                $return = $menu->add('Pengembalian', ['route' => 'return', 'class' => 'nav-item'])
                    ->prepend('<i class="nav-icon icon-arrow-right-circle"></i><span>')
                    ->append('</span>');
                $return->link->attr([
                        'class' =>'nav-link'
                    ]);

                $return->add('Add', ['route' => ['return.add', 'id' => $request->segment(3)], 'class' => 'breadcrumb-item']);
            });
        }

        return $next($request);
    }
}