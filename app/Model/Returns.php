<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Returns extends Model
{
    protected $table = 'returns';

    public function rent(){
        return $this->belongsTo("App\Model\Rent", "rent_id");
    }
}
