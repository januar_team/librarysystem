<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = ['name','nim','class_id'];

    public function class_room(){
        return $this->belongsTo("\App\Model\ClassRoom", "class_id");
    }
}
