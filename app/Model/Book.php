<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use SoftDeletes;

    const STATUS_AVAILABLE = 'available';
    const STATUS_UNAVAILABLE = 'unavailable';

    protected $fillable = ['isbn', 'code', 'title', 'author', 'publisher', 'unit'];

    protected $dates = ['deleted_at'];

    public function rents(){
        return $this->hasMany("\App\Model\Rent", "book_id");
    }

    public function rent(){
        return $this->hasOne("\App\Model\Rent", "book_id")->latest();
    }
}
