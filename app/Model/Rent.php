<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Rent extends Model
{
    protected $fillable = ['book_id', 'student_id', 'date'];

    public function book(){
        return $this->belongsTo("\App\Model\Book", "book_id")->withTrashed();
    }

    public function student(){
        return $this->belongsTo("\App\Model\Student", "student_id");
    }

    public function returns(){
        return $this->hasOne("\App\Model\Returns", "rent_id", "id");
    }
}
