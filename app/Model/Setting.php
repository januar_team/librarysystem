<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    const VALUE_TYPE = [
        'value_int' ,
        'value_float',
        'value_string',
        'value_date',
        'value_time',
        'value_datetime'
    ];

    public $timestamps = false;

    public static function get($name){
        $data = Setting::where('name', '=', $name)
            ->first();

        if ($data){
            return $data->{self::VALUE_TYPE[$data->value_type]};
        }

        return null;
    }
}
