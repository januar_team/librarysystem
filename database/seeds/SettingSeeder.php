<?php

use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\Setting::firstOrCreate(
            [
                'name' => 'Max_Rent_Time'
            ],
            [
                'value_type' => 0,
                'value_int' => 7
            ]
        );

        \App\Model\Setting::firstOrCreate(
            [
                'name' => 'Daily_Fine'
            ],
            [
                'value_type' => 0,
                'value_int' => 1000
            ]
        );
    }
}
