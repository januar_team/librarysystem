<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name");
            $table->integer('value_type');
            $table->integer('value_int')->nullable();
            $table->float('value_float')->nullable();
            $table->string('value_string')->nullable();
            $table->date('value_date')->nullable();
            $table->time('value_time')->nullable();
            $table->dateTime('value_datetime')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
