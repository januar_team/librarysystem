import $ from 'jquery'

const ButtonBs4 = (($) => {
    const NAME                  = 'button_bs4'
    const DATA_KEY              = 'bs.button'
    const DATA_LOADING_TEXT     = 'loadingText'
    const DATA_RESET_TEXT       = 'resetText'
    const DEFAULT_LOADING_TEXT  = 'loading ...'
    const DISABLED              = 'disabled'

    class ButtonBs4{
        constructor(element) {
            this._element = element
        }

        loading(){
            let data = this._element.data();
            if (data[DATA_RESET_TEXT] == null){
                this._element.data(DATA_RESET_TEXT, this._element.html())
            }

            this._element.html(data[DATA_LOADING_TEXT] == null ? DEFAULT_LOADING_TEXT : data[DATA_LOADING_TEXT])
            this._element.attr(DISABLED, DISABLED).prop(DISABLED, true);
        }

        reset(){
            let data = this._element.data();
            this._element.html(data[DATA_RESET_TEXT])
            this._element.removeAttr(DISABLED).prop(DISABLED, false);
        }

        static _jQueryInterface(config) {
            return this.each(function () {
                let data = $(this).data(DATA_KEY)

                if (!data) {
                    data = new ButtonBs4($(this))
                    $(this).data(DATA_KEY, data)
                }

                if (config === 'loading' || config === 'reset') {
                    data[config]()
                }
            })
        }
    }

    $.fn[NAME] = ButtonBs4._jQueryInterface

    return ButtonBs4
})($)

export default ButtonBs4