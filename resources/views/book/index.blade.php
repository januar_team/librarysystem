@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Simple Table

                    <div class="card-header-actions">
                        <a class="card-header-action btn-add" href="{{route('book.add')}}">
                            <i class="icon-plus"></i> tambah buku
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table id="tbl-book" class="table table-striped table-bordered datatable dataTable no-footer">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Judul</th>
                            <th>Pengarang</th>
                            <th>Penerbit</th>
                            <th>ISBN</th>
                            <th>Kode</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-delete" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form class="form-horizontal" action="{{route('book.delete')}}">
                    <div class="modal-header">
                        <h5 class="modal-title">Delete</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id" id="id">
                        <P>Are you want delete this item?</P>
                        <div id="error_message" class="form-group"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        jQuery(document).ready(function ($) {
            var table = $('#tbl-book').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    "url": "{{ route('book') }}",
                    "type": "POST",
                },
                columns: [
                    {data: null, "orderable": false, searchable: "false",
//                        render: function (data, type, row, meta) {
//                            return meta.row + 1;
//                        }
                    },
                    {data: 'title', name: 'title'},
                    {data: 'author'},
                    {data: 'publisher', name: 'publisher'},
                    {data: 'isbn', name: 'isbn'},
                    {data: 'code'},
                    {data: 'status', searchable: "false",
                        render: function (data, type, row, meta) {
                            return (data == "{{\App\Model\Book::STATUS_AVAILABLE}}")
                                ? '<span class="badge badge-success">'+ data +'</span>' :
                                '<span class="badge badge-danger">'+ data +'</span>';
                        }
                    },
                    {
                        data: null, searchable: "false",
                        render: function (data, type, row, meta) {
                            var group = '<div class="btn-group btn-group-sm" role="group" aria-label="Small button group">';
                            if(data.status === "unavailable"){
                                group += '<a href="/rent/detail/'+ data.rent.id +'" data-toggle="tooltip" title="Detail Peminjam" ' +
                                    'class="btn btn-brand btn-vine" data-id="' + data.id + '">' +
                                    '<i class="icons cui-people"></i></a>';
                            }else{
                                group += '<a href="/rent/add/?b=' + data.id + '" data-toggle="tooltip" title="Peminjaman buku" ' +
                                    'class="btn btn-brand btn-facebook"><i class="icon-arrow-left-circle"></i></a>';
                            }
                            group += '<a href="#" data-toggle="tooltip" title="Hapus buku" ' +
                                'class="btn btn-brand btn-youtube btn-delete" data-id="' + data.id + '">' +
                                '<i class="icons icon-trash"></i></a>';
                            group += '</div>';
                            return group;
                        },
                        "orderable": false,
                    }
                ],
                preDrawCallback: function(settings,json) {
                    if (settings.jqXHR != null)
                        settings.jqXHR.abort();
                },
                drawCallback: function (settings) {
                    $('[data-toggle="tooltip"]').tooltip();
                },
                "aoColumnDefs": [ {
                    "aTargets": [0],
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        if (iCol === 0) {
                            $(nTd).html(iRow + 1);
                        }
                    }
                }],
            });

            $('#tbl-book').on('click', '.btn-delete', function (event) {
                event.preventDefault();
                var id = $(this).data('id');
                $('#modal-delete form input#id').val(id);
                $('#modal-delete').modal();
            });

            $('#modal-delete form').submit(function (event) {
                event.preventDefault();
                var form = $(this);
                window.resetForm(form);
                var data = form.serialize();
                var submitBtn = form.find('button[type="submit"]');
                submitBtn.button_bs4('loading');
                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    success: function (data, textStatus, request) {
                        if (data.status) {
                            $('#modal-delete').modal('toggle');
                            table.draw();
                        } else {
                            window.handleError(data, form);
                        }
                    },
                    error: function (response, XMLHttpRequest, textStatus, errorThrown) {
                        window.handleError(response, form);
                    },
                    complete : function(xhr){
                        submitBtn.button_bs4('reset');
                    }
                });
            });
        });
    </script>
@endsection
