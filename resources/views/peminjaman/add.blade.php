@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <form action="{{route('rent.add')}}" method="post" class="form-horizontal" autocomplete="off">
                @csrf
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Peminjaman
                    </div>
                    <div class="card-body">
                        <h5 class="mini-title">
                            <i class="icon icon-book-open"></i> Data Buku
                        </h5>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Kode Buku</label>
                            <div class="col-md-9">
                                <select class="form-control{{ $errors->has('book_id') ? ' is-invalid' : '' }}"
                                       name="book_id" required></select>
                                @if ($errors->has('book_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('book_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Judul Buku</label>
                            <div class="col-md-9">
                                <p id="book-title" class="form-control-static">-</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Pengarang</label>
                            <div class="col-md-9">
                                <p id="book-author" class="form-control-static">-</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Penerbit</label>
                            <div class="col-md-9">
                                <p id="book-publisher" class="form-control-static">-</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">ISBN</label>
                            <div class="col-md-9">
                                <p id="book-isbn" class="form-control-static">-</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Status</label>
                            <div class="col-md-9">
                                <span class="badge badge-book-status">-</span>
                            </div>
                        </div>

                        <h5 class="mini-title">
                            <i class="icon icon-people"></i> Data Siswa (Peminjam)
                        </h5>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">NIM</label>
                            <div class="col-md-9">
                                <select class="form-control{{ $errors->has('student_id') ? ' is-invalid' : '' }}"
                                        name="student_id" required></select>
                                @if ($errors->has('student_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('student_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Nama</label>
                            <div class="col-md-9">
                                <p id="student-name" class="form-control-static">-</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Kelas</label>
                            <div class="col-md-9">
                                <p id="student-class" class="form-control-static">-</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Tanggal Peminjaman</label>
                            <div class="col-md-9">
                                <input class="form-control datepicker{{ $errors->has('date') ? ' is-invalid' : '' }}"
                                       name="date" value="{{ old('date') }}" type="text"
                                       placeholder="YYYY-MM-DD" data-date-autoclose="true" data-date-format="yyyy-mm-dd" data-date-end-date="{{date('Y-m-d')}}" required>
                                @if ($errors->has('date'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-sm btn-primary" type="submit">
                            <i class="fa fa-save"></i> Simpan
                        </button>
                        <a href="{{route('rent')}}" class="btn btn-sm btn-danger">
                            <i class="fa fa-arrow-left"></i> Kembali
                        </a>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('.datepicker').datepicker();

            var bookSelect2 = $('select[name="book_id"]').select2({
                minimumInputLength : 3,
                ajax:{
                    url : "{{route('book.search')}}",
                    dataType : "json",
                    data: function (params) {
                        var query = {
                            search: params.term,
                            page: params.page || 1
                        }
                        // Query parameters will be ?search=[term]&page=[page]
                        return query;
                    },
                    processResults : function (data) {
                        var items = [];
                        $.each(data.data, function (index, item) {
                           items.push(Object.assign(item, {text : item.code + " - " + item.title}))
                        });
                        return {
                            results: items,
                            pagination: {
                                more: (data.current_page < data.last_page)
                            }
                        };
                    }
                }
            }).on('select2:select', function (e) {
                var data = e.params.data;
                $('p#book-title').html(data.title);
                $('p#book-author').html(data.author);
                $('p#book-publisher').html(data.publisher);
                $('p#book-isbn').html(data.isbn);
                $('.badge-book-status').html(data.status);
                if(data.status === 'available'){
                    $('.badge-book-status').removeClass('badge-danger').addClass('badge-success');
                }else{
                    $('.badge-book-status').removeClass('badge-success').addClass('badge-danger');
                }
            });

            @if (old('book_id') != "")
            $.ajax({
                type: 'GET',
                url: "/book/get/{{ old('book_id') }}"
            }).then(function (data) {
                // create the option and append to Select2
                var option = new Option(data.code + " - " + data.title, data.id, true, true);
                bookSelect2.append(option).trigger('change');

                // manually trigger the `select2:select` event
                bookSelect2.trigger({
                    type: 'select2:select',
                    params: {
                        data: data
                    }
                });
            });
            @endif

            var studentSelect2 = $('select[name="student_id"]').select2({
                minimumInputLength : 3,
                ajax:{
                    url : "{{route('student.search')}}",
                    dataType : "json",
                    data: function (params) {
                        var query = {
                            search: params.term,
                            page: params.page || 1
                        };
                        // Query parameters will be ?search=[term]&page=[page]
                        return query;
                    },
                    processResults : function (data) {
                        var items = [];
                        $.each(data.data, function (index, item) {
                            items.push(Object.assign(item, {text : item.nim + " - " + item.name}))
                        });
                        return {
                            results: items,
                            pagination: {
                                more: (data.current_page < data.last_page)
                            }
                        };
                    }
                }
            }).on('select2:select', function (e) {
                var data = e.params.data;
                $('p#student-name').html(data.name);
                $('p#student-class').html(data.class_room.name);
            });

            @if (old('student_id') != "")
            $.ajax({
                type: 'GET',
                url: "/student/get/{{ old('student_id') }}"
            }).then(function (data) {
                // create the option and append to Select2
                var option = new Option(data.nim + " - " + data.name, data.id, true, true);
                studentSelect2.append(option).trigger('change');

                // manually trigger the `select2:select` event
                studentSelect2.trigger({
                    type: 'select2:select',
                    params: {
                        data: data
                    }
                });
            });
            @endif
        });
    </script>
@endsection