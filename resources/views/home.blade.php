@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-6 col-lg-3">
            <div class="card text-white bg-primary">
                <div class="card-body pb-0">
                    <div class="text-value">{{\App\Model\Book::count()}}</div>
                    <div>Jumlah buku</div>
                </div>
                <div class="chart-wrapper mt-3 mx-3" style="height:70px;">
                    <canvas class="chart" id="card-chart1" height="70"></canvas>
                </div>
            </div>
        </div>
        <!-- /.col-->
        <div class="col-sm-6 col-lg-3">
            <div class="card text-white bg-info">
                <div class="card-body pb-0">
                    <div class="text-value">{{\App\Model\Student::count()}}</div>
                    <div>Jumlah siswa</div>
                </div>
                <div class="chart-wrapper mt-3 mx-3" style="height:70px;">
                    <canvas class="chart" id="card-chart2" height="70"></canvas>
                </div>
            </div>
        </div>
        <!-- /.col-->
        <div class="col-sm-6 col-lg-3">
            <div class="card text-white bg-warning">
                <div class="card-body pb-0">
                    <div class="text-value">{{$active_rent}}</div>
                    <div>Peminjaman aktif</div>
                </div>
                <div class="chart-wrapper mt-3" style="height:70px;">
                    <canvas class="chart" id="card-chart3" height="70"></canvas>
                </div>
            </div>
        </div>
        <!-- /.col-->
        <div class="col-sm-6 col-lg-3">
            <div class="card text-white bg-danger">
                <div class="card-body pb-0">
                    <div class="text-value">{{$today_rent}}</div>
                    <div>Peminjaman hari ini</div>
                </div>
                <div class="chart-wrapper mt-3 mx-3" style="height:70px;">
                    <canvas class="chart" id="card-chart4" height="70"></canvas>
                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">Expired Today</h4>
                    <div class="small text-muted">{{\Carbon\Carbon::now()->format('d F Y')}}</div>
                </div>
            </div>

            <table class="table table-responsive-sm table-hover table-outline mb-0">
                <thead class="thead-light">
                <tr>
                    <th>Judul Buku</th>
                    <th>Pengarang</th>
                    <th>Peminjam</th>
                    <th>NIM</th>
                    <th>Tanggal Peminjaman</th>
                    <th>Jatuh Tempo</th>
                </tr>
                </thead>
                <tbody>
                @foreach($expired as $item)
                    <tr style="cursor: pointer" onclick="document.location.href = '{{route('rent.detail', ['id' => $item->id])}}'">
                        <td>{{$item->book->title}}</td>
                        <td>{{$item->book->author}}</td>
                        <td>{{$item->student->name}}</td>
                        <td>{{$item->student->nim}}</td>
                        <td>{{$item->date}}</td>
                        <td>
                            <span class="badge badge-normal
{{(\Carbon\Carbon::now()->diffInDays($item->expired_date) < 2)? 'badge-danger' :
(\Carbon\Carbon::now()->diffInDays($item->expired_date) < 4 ? 'badge-warning' : '')}}">
                                {{$item->expired_date}}
                            </span>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{mix('js/Chart.js')}}"></script>
    <script src="{{asset('js/widgets.js')}}"></script>
@endsection
