<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="{{ mix('css/bootstrap-datepicker.css') }}" rel="stylesheet"/>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
        <header class="app-header navbar">
            <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="#">
                <img class="navbar-brand-full" src="/img/brand/logo.svg" width="89" height="25" alt="Library System">
                <img class="navbar-brand-minimized" src="/img/brand/sygnet.svg" width="30" height="30" alt="Library System">
            </a>
            <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
                <span class="navbar-toggler-icon"></span>
            </button>
            <ul class="nav navbar-nav ml-auto">
                {{--<li class="nav-item d-md-down-none">--}}
                    {{--<a class="nav-link" href="#">--}}
                        {{--<i class="icon-bell"></i>--}}
                        {{--<span class="badge badge-pill badge-danger">5</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="nav-item d-md-down-none">--}}
                    {{--<a class="nav-link" href="#">--}}
                        {{--<i class="icon-list"></i>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="nav-item d-md-down-none">--}}
                    {{--<a class="nav-link" href="#">--}}
                        {{--<i class="icon-location-pin"></i>--}}
                    {{--</a>--}}
                {{--</li>--}}
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <img class="img-avatar" src="/img/avatars/6.jpg" alt="admin@bootstrapmaster.com">
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                        </form>
                        <div class="dropdown-header text-center">
                            <strong>Account</strong>
                        </div>
                        <a class="dropdown-item" href="#">
                            <i class="fa fa-key"></i> {{__('Change Password')}}</a>
                        <a class="dropdown-item" href="{{route('logout')}}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="fa fa-lock"></i> {{__('Logout')}}</a>
                    </div>
                </li>
            </ul>
        </header>

        <div class="app-body">
            <div class="sidebar">
                <nav class="sidebar-nav">
                    {!! $MyNavBar->asUl(
                     ['class' => 'nav'],
                     ['class' => 'nav-dropdown-items']
                     ) !!}
                </nav>
                <button class="sidebar-minimizer brand-minimizer" type="button"></button>
            </div>

            <main class="main">
                <!-- Breadcrumb-->
                {{--<ol class="breadcrumb">--}}
                    {{--<li class="breadcrumb-item">Home</li>--}}
                    {{--<li class="breadcrumb-item">--}}
                        {{--<a href="#">Admin</a>--}}
                    {{--</li>--}}
                    {{--<li class="breadcrumb-item active">Dashboard</li>--}}
                {{--</ol>--}}
                {!! Menu::get('MyNavBar')->crumbMenu()->asOl(
                    ['class' => 'breadcrumb'],[],
                    ['class' => 'breadcrumb-item'],
                    function ($item,
                        &$children_attributes,
                        &$item_attributes,
                        &$link_attr,
                        &$item_after_calback_params){
                        $item->title = strip_tags($item->title);
                        if ($item->isActive){
                            $item->attributes = ['class' => 'breadcrumb-item active'];
                        }
                    }
                ) !!}
                <div class="container-fluid">
                    <div class="animated fadeIn">

                        @yield('content')
                    </div>
                </div>
            </main>
        </div>
    <footer class="app-footer">
        <div>
            <a href="https://coreui.io">CoreUI</a>
            <span>&copy; 2018 creativeLabs.</span>
        </div>
        <div class="ml-auto">
            <span>Powered by</span>
            <a href="https://coreui.io">CoreUI</a>
        </div>
    </footer>

        <!-- Scripts -->
        <script src="{{ mix('js/app.js') }}" ></script>
        <script>
            jQuery(document).ready(function ($) {
                $('ol.breadcrumb').prepend('<li class="breadcrumb-item">Home</li>');
            })
        </script>
@yield('script')
</body>
</html>
