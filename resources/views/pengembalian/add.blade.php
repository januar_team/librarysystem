@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <form method="post" class="form-horizontal" autocomplete="off">
                @csrf
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Pengembalian
                    </div>
                    <div class="card-body">
                        <h5 class="mini-title">
                            <i class="icon icon-book-open"></i> Data Buku
                        </h5>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Kode Buku</label>
                            <div class="col-md-9">
                                <p id="book-code" class="form-control-static">: {{$rent->book->code}}</p>
                                <input type="hidden" name="rent_id" value="{{$rent->id}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Judul Buku</label>
                            <div class="col-md-9">
                                <p id="book-title" class="form-control-static">: {{$rent->book->title}}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Pengarang</label>
                            <div class="col-md-9">
                                <p id="book-author" class="form-control-static">: {{$rent->book->author}}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Penerbit</label>
                            <div class="col-md-9">
                                <p id="book-publisher" class="form-control-static">: {{$rent->book->publisher}}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">ISBN</label>
                            <div class="col-md-9">
                                <p id="book-isbn" class="form-control-static">: {{$rent->book->isbn}}</p>
                            </div>
                        </div>

                        <h5 class="mini-title">
                            <i class="icon icon-people"></i> Data Siswa (Peminjam)
                        </h5>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">NIM</label>
                            <div class="col-md-9">
                                <p id="student-name" class="form-control-static">: {{$rent->student->nim}}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Nama</label>
                            <div class="col-md-9">
                                <p id="student-name" class="form-control-static">: {{$rent->student->name}}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Kelas</label>
                            <div class="col-md-9">
                                <p id="student-class" class="form-control-static">: {{$rent->student->class_room->name}}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Tanggal Peminjaman</label>
                            <div class="col-md-9">
                                <p id="rent-date" class="form-control-static">: {{\Carbon\Carbon::createFromFormat("Y-m-d", $rent->date)->format("d F Y")}}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Tanggal Jatuh Tempo</label>
                            <div class="col-md-9">
                                <p id="rent-expired-date" class="form-control-static">:
                                    <span class="{{($fine > 0 ? 'badge badge-normal badge-danger' : '')}}">{{\Carbon\Carbon::createFromFormat("Y-m-d", $rent->expired_date)->format("d F Y")}}</span>
                                </p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Denda</label>
                            <div class="col-md-9">
                                <p id="rent-fine" class="form-control-static">: Rp.{{number_format($fine)}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-sm btn-primary" type="submit">
                            <i class="fa fa-save"></i> Simpan Pengembalian
                        </button>
                        <a href="{{route('rent')}}" class="btn btn-sm btn-danger">
                            <i class="fa fa-arrow-left"></i> Batal
                        </a>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
        });
    </script>
@endsection