@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Daftar Kelas

                    <div class="card-header-actions">
                        <a class="card-header-action btn-add" href="#">
                            <i class="icon-plus"></i> Add
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table id="tbl-class" class="table table-striped table-bordered datatable dataTable no-footer">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-add" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form class="form-horizontal">
                    <div class="modal-header">
                        <h5 class="modal-title">Add Class</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="name">Nama Kelas</label>
                            <div class="col-md-9">
                                <input type="hidden" name="id" id="id">
                                <input class="form-control" id="name" type="text" name="name" placeholder="Enter class name..">
                            </div>
                        </div>
                        <div id="error_message" class="form-group"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" data-loading-text="processing ...">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="modal-delete" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form class="form-horizontal" action="{{route('class.delete')}}">
                    <div class="modal-header">
                        <h5 class="modal-title">Delete</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id" id="id">
                        <P>Are you want delete this item?</P>
                        <div id="error_message" class="form-group"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        jQuery(document).ready(function ($) {
            var table = $('#tbl-class').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    "url": "{{ route('class') }}",
                    "type": "POST",
                },
                columns: [
                    {data: null, "orderable": false, searchable: "false"},
                    {data: 'name', name: 'name'},
                    {
                        data: null, searchable: "false",
                        render: function (data, type, row, meta) {
                            var group = '<div class="btn-group btn-group-sm" role="group" aria-label="Small button group">';
                            group += '<a href="#" data-toggle="tooltip" title="Edit" ' +
                                'class="btn btn-primary btn-edit"><i class="icons icon-pencil"></i></a>';
                            group += '<a href="#" data-toggle="tooltip" title="Hapus" ' +
                                'class="btn btn-primary btn-delete" data-id="' + data.id + '">' +
                                '<i class="icons icon-trash"></i></a>';
                            group += '</div>';
                            return group;
                        },
                        "orderable": false,
                    }
                ],
                preDrawCallback: function (settings, json) {
                    if (settings.jqXHR != null)
                        settings.jqXHR.abort();
                },
                drawCallback: function (settings) {
                    $('[data-toggle="tooltip"]').tooltip();
                },
                "fnCreatedRow": function(nRow, aData, iDataIndex) {
                    $(nRow).find('.btn-edit').data('item', JSON.stringify(aData));
                },
                "aoColumnDefs": [{
                    "aTargets": [0],
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        if (iCol === 0) {
                            $(nTd).html(iRow + 1);
                        }
                    }
                }],
            });

            $('.btn-add').click(function (event) {
                event.preventDefault();
                $('#modal-add').modal();
                $('#modal-add form').trigger("reset");
                window.resetForm($('#modal-add form'));
                $('#modal-add form').attr('action', "{{ route('class.add') }}");
            });

            $('#tbl-class').on('click', '.btn-edit', function (event) {
                event.preventDefault();
                var data = JSON.parse($(this).data('item'));
                $('#modal-add form input#id').val(data.id);
                $('#modal-add form input#name').val(data.name);
                $('#modal-add form').attr('action', "{{ route('class.edit') }}");
                $('#modal-add').modal();
            });

            $('#tbl-class').on('click', '.btn-delete', function (event) {
                event.preventDefault();
                var id = $(this).data('id');
                $('#modal-delete form input#id').val(id);
                $('#modal-delete').modal();
            });

            $('#modal-add form').submit(function (event) {
                event.preventDefault();
                var form = $(this);
                window.resetForm(form);
                var data = form.serialize();
                var submitBtn = form.find('button[type="submit"]');
                submitBtn.button_bs4('loading');
                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    success: function (data, textStatus, request) {
                        if (data.status) {
                            $('#modal-add').modal('toggle');
                            table.draw();
                        } else {
                            window.handleError(data, form);
                        }
                    },
                    error: function (response, XMLHttpRequest, textStatus, errorThrown) {
                        window.handleError(response, form);
                    },
                    complete : function(xhr){
                        submitBtn.button_bs4('reset');
                    }
                });
            });

            $('#modal-delete form').submit(function (event) {
                event.preventDefault();
                var form = $(this);
                window.resetForm(form);
                var data = form.serialize();
                var submitBtn = form.find('button[type="submit"]');
                submitBtn.button_bs4('loading');
                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    success: function (data, textStatus, request) {
                        if (data.status) {
                            $('#modal-delete').modal('toggle');
                            table.draw();
                        } else {
                            window.handleError(data, form);
                        }
                    },
                    error: function (response, XMLHttpRequest, textStatus, errorThrown) {
                        window.handleError(response, form);
                    },
                    complete : function(xhr){
                        submitBtn.button_bs4('reset');
                    }
                });
            });
        });
    </script>
@endsection
