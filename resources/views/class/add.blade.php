@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <form action="{{route('book.add')}}" method="post" class="form-horizontal" autocomplete="off">
                    @csrf
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Tambah Buku
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Judul</label>
                            <div class="col-md-9">
                                <input class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                       name="title" value="{{ old('title') }}" type="text"
                                       placeholder="Judul">
                                @if ($errors->has('title'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Penulis</label>
                            <div class="col-md-9">
                                <input class="form-control{{ $errors->has('author') ? ' is-invalid' : '' }}"
                                       name="author" value="{{ old('author') }}" type="text"
                                       placeholder="Penulis">
                                @if ($errors->has('author'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('author') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Penerbit</label>
                            <div class="col-md-9">
                                <input class="form-control{{ $errors->has('publisher') ? ' is-invalid' : '' }}"
                                       name="publisher" value="{{ old('publisher') }}" type="text"
                                       placeholder="Penerbit">
                                @if ($errors->has('publisher'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('publisher') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">ISBN</label>
                            <div class="col-md-3 col-sm-12">
                                <input class="form-control{{ $errors->has('isbn') ? ' is-invalid' : '' }}"
                                       name="isbn" value="{{ old('isbn') }}" type="text"
                                       placeholder="ISBN">
                                @if ($errors->has('isbn'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('isbn') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Kode</label>
                            <div class="col-md-3 col-sm-12">
                                <input class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}"
                                       name="code" value="{{ old('code') }}" type="text"
                                       placeholder="Kode">
                                @if ($errors->has('code'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Unit</label>
                            <div class="col-md-3 col-sm-12">
                                <input class="form-control{{ $errors->has('unit') ? ' is-invalid' : '' }}"
                                       name="unit" value="{{ old('unit') }}" type="number"
                                       placeholder="Unit">
                                @if ($errors->has('unit'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('unit') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-sm btn-primary" type="submit">
                            <i class="fa fa-save"></i> Simpan
                        </button>
                        <a href="{{route('book')}}" class="btn btn-sm btn-danger">
                            <i class="fa fa-arrow-left"></i> Kembali
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
