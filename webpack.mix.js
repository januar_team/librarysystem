const mix = require('laravel-mix');
const webpack = require('webpack');

// mix.webpackConfig({
//     plugins: [
//         new webpack.ContextReplacementPlugin(
//             /moment[\/\\]locale$/,
//             // A regular expression matching files that should be included
//             /id\.js/
//         )
//     ]
// });

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('node_modules/chart.js/dist/Chart.bundle.js', 'public/js/Chart.js')
   .sass('resources/sass/app.scss', 'public/css')
    .less('node_modules/bootstrap-datepicker/build/build_standalone.less', 'public/css/bootstrap-datepicker.css');
