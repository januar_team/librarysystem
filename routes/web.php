<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/', 'HomeController@index')->name('home');

    // Book
    Route::match(['get', 'post'], '/book', 'BookController@index')->name('book');
    Route::match(['get', 'post'], '/book/add', 'BookController@add')->name('book.add');
    Route::get('/book/search', 'BookController@search')->name('book.search');
    Route::post('/book/delete', 'BookController@delete')->name('book.delete');
    Route::get('/book/get/{id}', 'BookController@get')->name('book.get');

    // Class
    Route::match(['get', 'post'], '/class', 'ClassController@index')->name('class');
    Route::post('/class/add', 'ClassController@add')->name('class.add');
    Route::post('/class/edit', 'ClassController@edit')->name('class.edit');
    Route::post('/class/delete', 'ClassController@delete')->name('class.delete');

    // Student
    Route::match(['get', 'post'], '/student', 'StudentController@index')->name('student');
    Route::post('/student/add', 'StudentController@add')->name('student.add');
    Route::post('/student/edit', 'StudentController@edit')->name('student.edit');
    Route::post('/student/delete', 'StudentController@delete')->name('student.delete');
    Route::get('/student/search', 'StudentController@search')->name('student.search');
    Route::get('/student/get/{id}', 'StudentController@get')->name('student.get');

    // Peminjaman
    Route::match(['get', 'post'], '/rent', 'PeminjamanController@index')->name('rent');
    Route::match(['get', 'post'], '/rent/add', 'PeminjamanController@add')->name('rent.add');
    Route::get('/rent/detail/{id}', 'PeminjamanController@detail')->name('rent.detail');
    Route::post('/rent/delete', 'PeminjamanController@delete')->name('rent.delete');

    // Pengembalian
    Route::match(['get', 'post'], '/return', 'PengembalianController@index')->name('return');
    Route::match(['get', 'post'], '/return/add/{id}', 'PengembalianController@add')->name('return.add');
});
